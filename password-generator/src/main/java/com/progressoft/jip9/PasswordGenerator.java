package com.progressoft.jip9;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class PasswordGenerator {
    private static final char[] SPECIAL = {'_', '$', '#', '%'};
    private static final char[] ALPHABETS = new char[26];
    private static final char[] DIGITS = new char[10];
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();

    static {
        for (int i = 0; i < 26; i++) {
            ALPHABETS[i] = (char)('A' + i);
        }
        for (int i = 0; i < 10; i++) {
            DIGITS[i] = (char)('0' + i);
        }
    }

    public Random random;

    public PasswordGenerator() {
        random = new Random(SECURE_RANDOM.nextLong());
    }

    public String generatePassword() {
        ArrayList<Character> passChars = new ArrayList<>();

        select(passChars, ALPHABETS, 4);
        select(passChars, DIGITS, 2);
        select(passChars, SPECIAL, 2);
        Collections.shuffle(passChars, random);

        StringBuilder sb = new StringBuilder();
        for (char c : passChars)
            sb.append(c);
        return sb.toString();
    }

    private void select(ArrayList<Character> chosen, char[] selection, int count) {
        for (int i = 0; i < count; i++) {
            chosen.add(selection[random.nextInt(selection.length)]);
        }
    }
}
