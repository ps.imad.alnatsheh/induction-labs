package com.progressoft.jip9;

import java.util.Arrays;
import java.util.TreeSet;

import static com.progressoft.jip9.MatrixMath.*;

public class Main {
    public static void main(String[] args) {
	    double[][] a = {{1,2,3},{4,5,6}};
	    double[][] b = {{6,3},{5,2},{4,1}};
        double[][] c = {{1,2,3,14,5},{7,8,9,11,2},{3,3,2,2,1},{1,2,3,4,5},{2,2,2,5,2}};

        System.out.println("a=");
        print(a);
        System.out.println("\nb=");
        print(b);
        System.out.println("\nc=");
        print(c);
        System.out.println("\nc diagonal=");
        print(toDiagonal(c));
        System.out.println("\nc lower triangular=");
        print(toLowerTriangular(c));
        System.out.println("\nc upper triangular=");
        print(toUpperTriangular(c));
        System.out.println("\na + (b transposed) =");
        print(sum(a,transpose(b)));
        System.out.println("\n3.5 * a =");
        print(scale(a,3.5));
        System.out.println("\na * b =");
        print(matrixMul(a,b));
        System.out.println("\nb * a =");
        print(matrixMul(b,a));
        System.out.printf("\n|b * a| = %f\n", determinant(matrixMul(b,a)));
        System.out.println("\nc without row 2, or col 4 =");
        print(submatrix(c,2,4));
        System.out.println("\nc without row 2/3, or col 1/4 =");
        print(submatrix(c, new TreeSet<>(Arrays.asList(2, 3)),new TreeSet<>(Arrays.asList(1, 4))));

    }
}
