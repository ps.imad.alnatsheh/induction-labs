package com.progressoft.jip9;

import java.util.*;

public class MatrixMath {

    private static double[][] copy(double[][] m) {
        if (isEmptyMatrix(m))
            return new double[0][0];
        double[][] result = new double[m.length][m[0].length];
        for (int i = 0; i < result.length; ++i)
            for (int j = 0; j <= result[0].length; ++j)
                result[i][j] = m[i][j];
        return result;
    }


    public static double[][] sum(double[][] a, double[][] b) {
        if (isEmptyMatrix(a) && isEmptyMatrix(b))
            return new double[0][0];
        if (!isConsistent(a) || !isConsistent(b)) {
            throw new IllegalArgumentException("Inconsistent rows");
        }
        if (a.length != b.length || a[0].length != b[0].length) {
            throw new IllegalArgumentException("Inconsistent matrices, should have the same dimensions");
        }
        double[][] result = new double[a.length][a[0].length];
        for (int row = 0; row < result.length; ++row)
            for (int col = 0; col < result[0].length; ++col) {
                result[row][col] = a[row][col] + b[row][col];
            }
        return result;
    }

    public static double[][] scale(double[][] m, double s) {
        if (isEmptyMatrix(m)) {
            return new double[0][0];
        }
        throwIfInconsistent(m);

        double[][] result = new double[m.length][m[0].length];
        for (int i = 0; i < result.length; ++i)
            for (int j = 0; j < result[0].length; ++j) {
                result[i][j] = s * m[i][j];
            }
        return result;
    }

    public static double[][] matrixMul(double[][] a, double[][] b) {
        if (isEmptyMatrix(a) && isEmptyMatrix(b))
            return new double[0][0];

        throwIfInconsistent(a);
        throwIfInconsistent(b);

        if (a.length != b[0].length || a[0].length != b.length) {
            throw new IllegalArgumentException("Matrices must be of matching sizes to multiply.");
        }
        double[][] result = new double[a.length][b[0].length];
        for (int i = 0; i < result.length; ++i) {
            for (int j = 0; j < result[0].length; ++j) {
                double sum = 0;
                for (int k = 0; k < a[0].length; ++k) {
                    sum += a[i][k] * b[k][j];
                }
                result[i][j] = sum;
            }
        }
        return result;
    }


    public static double[][] submatrix(double[][] m, SortedSet<Integer> rowsToRemove, SortedSet<Integer> colsToRemove) {
        if (isEmptyMatrix(m)) {
            return new double[0][0];
        }
        throwIfInconsistent(m);

        if(rowsToRemove.first() < 0 || rowsToRemove.last() >= m.length)
            throw new IndexOutOfBoundsException("One or more rows supplied is out of bounds.");
        if(colsToRemove.first() < 0 || colsToRemove.last() >= m.length)
            throw new IndexOutOfBoundsException("One or more columns supplied is out of bounds.");

        List<Integer> rowsToKeep = new ArrayList<>();
        List<Integer> colsToKeep = new ArrayList<>();
        for (int i = 0; i < m.length; ++i) {
            if (!rowsToRemove.contains(i))
                rowsToKeep.add(i);
        }
        for (int i = 0; i < m[0].length; ++i) {
            if (!colsToRemove.contains(i))
                colsToKeep.add(i);
        }
        double[][] result = new double[rowsToKeep.size()][colsToKeep.size()];
        for (int row = 0; row < result.length; ++row) {
            for (int col = 0; col < result[0].length; ++col) {
                result[row][col] = m[rowsToKeep.get(row)][colsToKeep.get(col)];
            }
        }
        return result;
    }

    public static double[][] submatrix(double[][] m, int rowToRemove, int colToRemove) {
        return submatrix(m, new TreeSet<>(Collections.singletonList(rowToRemove)),new TreeSet<>(Collections.singletonList(colToRemove)));
    }

    public static double[][] toDiagonal(double[][] m) {
        if (isEmptyMatrix(m)) {
            return new double[0][0];
        }
        if (!isSquareMatrix(m)) {
            throw new IllegalArgumentException("Matrix must be square");
        }
        throwIfInconsistent(m);
        double[][] result = new double[m.length][m.length];
        for (int i = 0; i < result.length; ++i)
            result[i][i] = m[i][i];
        return result;
    }


    public static double[][] toLowerTriangular(double[][] m) {
        if (isEmptyMatrix(m)) {
            return new double[0][0];
        }
        if (!isSquareMatrix(m)) {
            throw new IllegalArgumentException("Matrix must be square");
        }
        throwIfInconsistent(m);
        double[][] result = new double[m.length][m.length];
        for (int row = 0; row < result.length; ++row)
            for (int col = 0; col <= row; ++col)
                result[row][col] = m[row][col];
        return result;
    }

    public static double[][] toUpperTriangular(double[][] m) {
        if (isEmptyMatrix(m)) {
            return new double[0][0];
        }
        if (!isSquareMatrix(m)) {
            throw new IllegalArgumentException("Matrix must be square");
        }
        throwIfInconsistent(m);
        double[][] result = new double[m.length][m.length];
        for (int row = 0; row < result.length; ++row)
            for (int col = row; col < result.length; ++col)
                result[row][col] = m[row][col];
        return result;
    }

    public static double determinant(double[][] m) {
        if (isEmptyMatrix(m)) {
            return 0;
        }
        if (!isSquareMatrix(m)) {
            throw new IllegalArgumentException("Matrix must be square");
        }
        throwIfInconsistent(m);
        if (m.length == 1)
            return m[0][0];
        return determinantRec(m);
    }

    public static double[][] transpose(double[][] m) {
        if (isEmptyMatrix(m)) {
            return new double[0][0];
        }
        throwIfInconsistent(m);

        double[][] transposed = new double[m[0].length][m.length];
        for (int i = 0; i < transposed.length; ++i)
            for (int j = 0; j < transposed[0].length; ++j) {
                transposed[i][j] = m[j][i];
            }
        return transposed;
    }

    public static void print(double[][] m) {
        for (int i = 0; i < m.length; ++i) {
            for (int j = 0; j < m[i].length; ++j) {
                System.out.print(m[i][j]);
                System.out.print(' ');
            }
            System.out.println();
        }
    }

    private static boolean isSquareMatrix(double[][] m) {
        return m.length == m[0].length;
    }

    private static boolean isEmptyMatrix(double[][] m) {
        return m.length == 0;
    }

    private static double determinantRec(double[][] m) {
        if (m.length == 2)
            return m[0][0] * m[1][1] - m[0][1] * m[1][0];
        double sign = 1;
        double sum = 0;
        for (int i = 0; i < m.length; ++i) {
            sum += sign * m[0][i] * determinantRec(submatrix(m, 0, i));
            sign = -sign;
        }
        return sum;
    }

    /**
     * Checks if the given matrix has all rows with the same size.
     */
    private static boolean isConsistent(double[][] m) {
        if (isEmptyMatrix(m))
            return false;
        int len = m[0].length;
        for (int i = 1; i < m.length; ++i)
            if (m[i].length != len)
                return false;
        return true;
    }

    private static void throwIfInconsistent(double[][] m) {
        if (!isConsistent(m)) {
            throw new IllegalArgumentException("Matrix must have equal row sizes for all rows.");
        }
    }
}
