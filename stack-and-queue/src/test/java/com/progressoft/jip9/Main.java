package com.progressoft.jip9;

public class Main {
    public static void main(String[] args) {
        Stack stack = new Stack();
        System.out.println("Stack:");
        stack.push(2);
        stack.push(3.7);
        stack.push(5.1);
        stack.push(5.1);
        stack.push(9.1);
        stack.printContents();
        stack.pop();
        System.out.println("\nElement popped. Peek result:");
        System.out.println(stack.peek());
        stack.pop();
        System.out.println("\nElement popped. New stack:");
        stack.printContents();
        stack.pop();
        stack.pop();
        System.out.println("\nTwo elements popped. New size:");
        System.out.println(stack.size());
        System.out.println("\nIs stack empty?");
        System.out.println(stack.isEmpty());

        Queue queue = new Queue();
        queue.enqueue(2);
        queue.dequeue();
        queue.enqueue(3.7);
        queue.enqueue(5.1);
        queue.enqueue(5.1);
        queue.enqueue(3);
        queue.enqueue(9.1);
        System.out.println("\n\nQueue:");
        queue.printContents();
        queue.dequeue();
        System.out.println("\nElement dequeued. Queue peek:");
        System.out.println(queue.peek());
        queue.dequeue();
        System.out.println("\nElement dequeued. New queue:");
        queue.printContents();
        queue.dequeue();
        queue.dequeue();
        System.out.println("\n2 Elements dequeued. New queue:");
        queue.printContents();
    }
}
