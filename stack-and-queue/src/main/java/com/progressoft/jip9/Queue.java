package com.progressoft.jip9;

public class Queue extends ArrayStore {

    /**
     * Creates a dynamic queue
     */
    public Queue() {
        this(-1);
    }

    /**
     * Creates a queue. If the specified size is non-negative the queue will be fixed, otherwise, the stack will be dynamic.
     *
     * @param fixedSize determines the size for a fixed queue, input a negative number for a dynamic queue
     */
    public Queue(int fixedSize) {
        super(fixedSize);
    }

    public void enqueue(double element) {
        if (isFull())
            expand();

        if (end == data.length)
            shiftElements();

        data[end] = element;
        ++end;
    }

    public double dequeue() {
        double value = peek();
        ++start;
        tryShrink();
        return value;
    }

    public double peek() {
        if (isEmpty()) {
            throw new IllegalStateException("Cannot peek into an empty queue.");
        }
        return data[start];
    }

    public void printContents() {
        System.out.print("head --> ");
        System.out.print(toString());
        System.out.println("<-- end");
    }
}
