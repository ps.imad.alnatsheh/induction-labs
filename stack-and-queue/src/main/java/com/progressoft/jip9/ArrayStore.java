package com.progressoft.jip9;

public abstract class ArrayStore {
    protected static final int MIN_SIZE = 50;
    private final boolean isFixed;
    protected int start;
    protected int end;
    protected double[] data;

    protected ArrayStore(int fixedSize) {
        if (fixedSize < 0) {
            data = new double[MIN_SIZE];
            isFixed = false;
        } else {
            data = new double[fixedSize];
            isFixed = true;
        }
        start = end = 0;
    }

    public boolean isFixed() {
        return isFixed;
    }

    public boolean isFull() {
        return size() == data.length;
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public int size() {
        return end - start;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = start; i < end; ++i) {
            sb.append(data[i]);
            sb.append(' ');
        }
        return sb.toString();
    }

    public abstract void printContents();

    public abstract double peek();

    protected boolean tryShrink() {
        if (isFixed())
            return false;
        int newSize = data.length / 4;
        if (newSize < MIN_SIZE)
            newSize = MIN_SIZE;
        if (size() < newSize && newSize != data.length) {
            double[] newData = new double[newSize];
            System.arraycopy(data, start, newData, 0, size());
            data = newData;
            end -= start;
            start = 0;
            return true;
        }
        return false;
    }

    protected void shiftElements() {
        if (start > 0) {
            for (int i = start, j = 0; i < end; ++i, ++j)
                data[j] = data[i];
            end -= start;
            start = 0;
        }
    }

    protected void expand() {
        if (isFixed())
            throw new IllegalStateException("Cannot expand the size of a fixed Stack.");
        int newSize = data.length * 2;
        if (newSize == data.length)
            ++newSize;
        double[] newData = new double[newSize];
        System.arraycopy(data, 0, newData, 0, data.length);
        data = newData;
    }
}
