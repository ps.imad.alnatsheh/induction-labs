package com.progressoft.jip9;

public class Stack extends ArrayStore {

    /**
     * Creates a dynamic stack
     */
    public Stack() {
        this(-1);
    }

    /**
     * Creates a stack. If the specified size is non-negative the stack will be fixed, otherwise, the stack will be dynamic.
     *
     * @param fixedSize determines the size for a fixed stack, input a negative number for a dynamic stack
     */
    public Stack(int fixedSize) {
        super(fixedSize);
    }

    public void push(double element) {
        if (isFull())
            expand();
        data[end] = element;
        ++end;
    }

    public double pop() {
        double value = peek();
        --end;
        tryShrink();
        return value;
    }

    public double peek() {
        if (isEmpty()) {
            throw new IllegalStateException("Cannot peek into an empty stack");
        }
        return data[end - 1];
    }

    public void printContents() {
        System.out.print("bottom --> ");
        System.out.print(toString());
        System.out.println("<-- top");
    }

}
