package com.progressoft.jip9;

public class Main {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();
        System.out.printf("Tree depth: %d\n",tree.treeDepth());
        tree.depth(10);
        tree.accept(10);
        System.out.printf("Added 10. Tree depth: %d\n",tree.treeDepth());
        tree.accept(6);
        tree.accept(18);
        System.out.printf("Added 6,18. Tree depth: %d\n",tree.treeDepth());
        tree.accept(4);
        tree.accept(8);
        tree.accept(15);
        tree.accept(21);
        System.out.printf("Added 4,8,15,21. Tree depth: %d\n\n",tree.treeDepth());
        int[] values = {10, 6, 18,13, 4, 8,5, 15, 21};
        for(int v : values) {
            System.out.printf("Depth of %d: %d\n",v,tree.depth(v));
        }
    }
}
