package com.progressoft.jip9;

public class BinaryTree {
    private Node root;

    public BinaryTree() {
        root = new EmptyNode();
    }

    public boolean accept(int value) {
        return root.accept(value);
    }

    public int depth(int value) {
        return root.depth(value);
    }

    public int treeDepth() {
        return root.treeDepth();
    }

    private class EmptyNode extends Node {
        @Override
        public boolean accept(int value) {
            root = new Node(value);
            return true;
        }

        @Override
        public int treeDepth() {
            return 0;
        }

        @Override
        public int depth(int value) {
            return 0;
        }
    }

    private static class Node {
        int data;
        Node left, right;

        public Node(){}
        public Node(int value) {
            data = value;
        }

        public boolean accept(int value) {
            if (value == data)
                return false;
            if (value > data)
                return acceptToRight(value);
            return acceptToLeft(value);
        }

        private boolean acceptToLeft(int value) {
            if (left == null) {
                left = new Node(value);
                return true;
            }
            return left.accept(value);
        }

        private boolean acceptToRight(int value) {
            if (right == null) {
                right = new Node(value);
                return true;
            }
            return right.accept(value);
        }

        public int treeDepth() {
            int ldepth = left == null ? 0 : left.treeDepth();
            int rdepth = right == null ? 0 : right.treeDepth();
            return 1 + Math.max(ldepth, rdepth);
        }

        public int depth(int value) {
            if (value == data)
                return 1;
            if (value > data) {
                return rightDepth(value);
            }
            return leftDepth(value);
        }

        private int leftDepth(int value) {
            return valueDepth(value, left);
        }

        private int rightDepth(int value) {
            return valueDepth(value, right);
        }

        private int valueDepth(int value, Node node) {
            if (node == null) {
                return 0;
            }
            int depth = node.depth(value);
            return depth > 0 ? depth + 1 : 0;
        }
    }
}

