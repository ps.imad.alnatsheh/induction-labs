package com.progressoft.jip9.operator;

public class OperatorDivide extends Operator {
    OperatorDivide() {
        super(2, "/");
    }

    @Override
    public double doOperation(double left, double right) {
        return left / right;
    }
}