package com.progressoft.jip9.operator;

public class OperatorPower extends Operator {
    OperatorPower() {
        super(3, "^");
    }

    @Override
    public double doOperation(double left, double right) {
        return Math.pow(left,right);
    }
}