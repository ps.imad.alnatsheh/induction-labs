package com.progressoft.jip9.operator;

public class OperatorMinus extends Operator {
    OperatorMinus() {
        super(1, "-");
    }

    @Override
    public double doOperation(double left, double right) {
        return left - right;
    }
}