package com.progressoft.jip9.operator;

public class OperatorPlus extends Operator {
    OperatorPlus() {
        super(1, "+");
    }

    @Override
    public double doOperation(double left, double right) {
        return left + right;
    }
}
