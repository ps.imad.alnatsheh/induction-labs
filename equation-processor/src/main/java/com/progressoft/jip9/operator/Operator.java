package com.progressoft.jip9.operator;

public abstract class Operator {
    private int priority;
    private String symbol;

    Operator(int priority, String symbol) {
        this.priority = priority;
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getPriority() {
        return priority;
    }

    public abstract double doOperation(double left, double right);
}
