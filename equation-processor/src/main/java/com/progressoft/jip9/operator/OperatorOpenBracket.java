package com.progressoft.jip9.operator;

public class OperatorOpenBracket extends Operator {
    OperatorOpenBracket() {
        super(0, "(");
    }

    @Override
    public double doOperation(double left, double right) {
        return 0;
    }
}