package com.progressoft.jip9.operator;

import java.util.HashMap;
import java.util.Map;

public class Operators {
    private static Map<String,Operator> strToOp = new HashMap<>();
    public static final OperatorPlus OPERATOR_PLUS = new OperatorPlus();
    public static final OperatorMinus OPERATOR_MINUS = new OperatorMinus();
    public static final OperatorMultiply OPERATOR_MULTIPLY = new OperatorMultiply();
    public static final OperatorDivide OPERATOR_DIVIDE = new OperatorDivide();
    public static final OperatorPower OPERATOR_POWER = new OperatorPower();
    public static final OperatorOpenBracket OPERATOR_OPEN_BRACKET = new OperatorOpenBracket();
    public static final OperatorMod OPERATOR_MOD = new OperatorMod();

    static {
        strToOp.put(OPERATOR_PLUS.getSymbol(),OPERATOR_PLUS);
        strToOp.put(OPERATOR_MINUS.getSymbol(),OPERATOR_MINUS);
        strToOp.put(OPERATOR_MULTIPLY.getSymbol(),OPERATOR_MULTIPLY);
        strToOp.put(OPERATOR_DIVIDE.getSymbol(),OPERATOR_DIVIDE);
        strToOp.put(OPERATOR_POWER.getSymbol(),OPERATOR_POWER);
        strToOp.put(OPERATOR_OPEN_BRACKET.getSymbol(),OPERATOR_OPEN_BRACKET);
        strToOp.put(OPERATOR_MOD.getSymbol(),OPERATOR_MOD);
    }

    public static Operator getOperator(String symbol) {
        if (!strToOp.containsKey(symbol))
            throw new IllegalArgumentException("Operator '" + symbol + "' was not recognized");
        return strToOp.get(symbol);
    }
}
