package com.progressoft.jip9.operator;

public class OperatorMod extends Operator {
    OperatorMod()  {
        super(1, "%");
    }

    @Override
    public double doOperation(double left, double right) {
        return left % right;
    }
}
