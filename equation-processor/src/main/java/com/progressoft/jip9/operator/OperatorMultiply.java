package com.progressoft.jip9.operator;

public class OperatorMultiply extends Operator {
    OperatorMultiply() {
        super(2, "*");
    }

    @Override
    public double doOperation(double left, double right) {
        return left * right;
    }
}