package com.progressoft.jip9;

import java.util.Scanner;

public class ConsoleUserInterface implements VariableSupplier {
    Scanner inScanner;

    public ConsoleUserInterface() {
        inScanner = new Scanner(System.in);
    }

    public void start() {
        while (true) {
            System.out.println("Enter the equation in a line (or write 'exit' to close the program):");
            String equation = inScanner.nextLine();
            while (equation.trim().isEmpty())
                equation = inScanner.nextLine();
            if (equation.equals("exit"))
                break;
            double result = EquationProcessor.processEquation(equation,this);
            System.out.printf("Result is: %.2f\n", result);
        }
    }

    @Override
    public double getValueOf(String variable) {
        System.out.printf("Input the value of the variable '%s':", variable);
        while (!inScanner.hasNextDouble())
            if (inScanner.hasNext()) {
                inScanner.next();
                System.out.print("\nPlease input a valid value:");
            }
        return Double.parseDouble(inScanner.nextLine()); // avoid empty line in buffer from nextDouble
    }
}
