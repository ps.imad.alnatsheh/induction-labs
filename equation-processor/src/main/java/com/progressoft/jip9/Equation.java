package com.progressoft.jip9;

import com.progressoft.jip9.operator.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public class Equation {
    public Stack<Double> operands = new Stack<>();
    public Stack<Operator> operators = new Stack<>();
    public Map<String, Double> variables = new HashMap<>();

    VariableSupplier supplier;
    public Equation(VariableSupplier supplier) {
        this.supplier = supplier;
    }

    public void pushOperand(double op) {
        operands.push(op);
    }

    public void pushVar(String var) {
        if (!variables.containsKey(var))
            variables.put(var, supplier.getValueOf(var));
        pushOperand(variables.get(var));
    }

    public void pushOperator(String op) {
        if (op.equals("(")) {
            operators.push(Operators.OPERATOR_OPEN_BRACKET);
            return;
        }
        if (op.equals(")")) {
            calculateToOpenBracket();
            return;
        }
        pushByPriority(op);
    }

    private void pushByPriority(String opString) {
        Operator operator = Operators.getOperator(opString);
        while (!operators.empty() && operator.getPriority() < operators.peek().getPriority()) {
            popOperator();
        }
        operators.push(operator);
    }

    private void calculateToOpenBracket() {
        while (true) {
            if (operators.isEmpty())
                throw new IllegalStateException("could not find the closing bracket");
            if (operators.peek() == Operators.OPERATOR_OPEN_BRACKET) {
                operators.pop();
                break;
            }
            popOperator();
        }
    }

    public double popRest() {
        while (!operators.empty())
            popOperator();
        if (operands.size() > 1)
            throw new IllegalArgumentException("Could not parse equation, not enough operators");
        return operands.pop();
    }

    private void popOperator() {
        Operator op = operators.pop();
        if (operands.size() < 2)
            throw new RuntimeException("Could not parse equation, not enough operands");
        double op2 = operands.pop();
        double op1 = operands.pop();
        pushOperand(op.doOperation(op1,op2));
    }
}
