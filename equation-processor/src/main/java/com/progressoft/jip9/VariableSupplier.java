package com.progressoft.jip9;

public interface VariableSupplier {
    double getValueOf(String variable);
}
