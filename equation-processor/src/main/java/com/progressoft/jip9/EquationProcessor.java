package com.progressoft.jip9;

import java.util.regex.Pattern;

public class EquationProcessor {
    public static final String operatorRegex = "[+\\-*/^()%]";
    public static final String doubleRegex = "-?\\d+(\\.\\d*)?"; //basic but does the job

    public static double processEquation(String equation, VariableSupplier supplier) {
        equation = prepareEquation(equation);
        String[] parts = equation.split("\\s+");
        Equation equ = new Equation(supplier);
        for (String part : parts) {
            if (part.isEmpty())
                continue;
            if (isNumber(part)) {
                equ.pushOperand(Double.parseDouble(part));
                continue;
            }
            if (isOperator(part)) {
                equ.pushOperator(part);
                continue;
            }
            equ.pushVar(part);
        }
        return equ.popRest();
    }

    private static String prepareEquation(String equation) {
        equation = putSpaceBeforeDigits(equation);
        equation = putSpaceBeforeOperators(equation);
        equation = putSpaceBeforeVariables(equation);
        return equation;
    }

    private static String putSpaceBeforeVariables(String equation) {
        return equation.replaceAll("(\\s*[^\\d.+\\-*/^()%]+)", " $1");
    }

    private static String putSpaceBeforeOperators(String equation) {
        return equation.replaceAll("(\\s*[+\\-*/^()%])", " $1");
    }

    private static String putSpaceBeforeDigits(String equation) {
        // avoid separating sign by lookback. A '-' after closing bracket is always minus.
        return equation.replaceAll("(\\s*((?<=[+\\-*/^(%])\\s*-|^-)?\\d+(\\.\\d*)?)", " $1");
    }

    private static boolean isOperator(String part) {
        return Pattern.matches(operatorRegex, part);
    }

    private static boolean isNumber(String part) {
        return Pattern.matches(doubleRegex, part);
    }

}

